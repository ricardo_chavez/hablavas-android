package ${basePackage}.adapter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.devos.android.image.ImageLoader;
import ${basePackage}.R;
import ${basePackage}.model.bean.${entidad};

public class Listado${entidad}Adapter extends BaseAdapter implements Filterable {
	protected LayoutInflater mInflater;
	private List<${entidad}> mListado${entidad};
	private List<${entidad}> mListado${entidad}Inicial;
	private ${entidad}Filter m${entidad}Filter;
	private ImageLoader imageLoader;

	public Listado${entidad}Adapter(Context context, List<${entidad}> listado${entidad}) {
		mInflater = LayoutInflater.from(context);
		mListado${entidad} = mListado${entidad}Inicial = listado${entidad};
		int imageSize = 100;
		imageLoader = new ImageLoader(context,android.R.drawable.stat_notify_sync,imageSize);
	}

	public int getCount() {
		return mListado${entidad}.size();
	}

	public ${entidad} getItem(int position) {
		return mListado${entidad}.get(position);
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_${entidad?lower_case}, null);
			viewHolder = new ViewHolder();
			viewHolder.tviTitulo = (TextView) convertView
					.findViewById(R.id.tviTitulo);
			viewHolder.iviImagen = (ImageView) convertView
					.findViewById(R.id.iviImagen);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.tviTitulo.setText(mListado${entidad}.get(position).get${campoTitulo?string[0]?upper_case + campoTitulo?substring(1)}());
		
		String fecha = Util.getFecha(mListado${entidad}.get(position)
				.getFechaActualizacionImagen());

		imageLoader.DisplayImage(GestorPrincipal.IMAGES_URL_BASE
				+ mListado${entidad}.get(position).getUrlImagen() + fecha,
				viewHolder.iviImagen);		
		
		return convertView;
	}

	public static class ViewHolder {
		TextView tviTitulo;
		ImageView iviImagen;
	}

	@Override
	public Filter getFilter() {
		if (m${entidad}Filter == null) {
			m${entidad}Filter = new ${entidad}Filter();
		}
		return m${entidad}Filter;
	}

	private class ${entidad}Filter extends Filter {
		@SuppressLint("DefaultLocale")
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults results = new FilterResults();
			if (constraint == null || constraint.length() == 0) {
				// No filter implemented we return all the list
				results.values = mListado${entidad}Inicial;
				results.count = mListado${entidad}Inicial.size();
			} else {
				// We perform filtering operation
				List<${entidad}> nListado${entidad} = new ArrayList<${entidad}>();

				for (${entidad} e : mListado${entidad}Inicial) {
					if (e.get${campoTitulo?cap_first}().toUpperCase()
							.startsWith(constraint.toString().toUpperCase())) {
						nListado${entidad}.add(e);
					}
				}
				results.values = nListado${entidad};
				results.count = nListado${entidad}.size();
			}
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			if (results.count == 0)
				notifyDataSetInvalidated();
			else {
				mListado${entidad} = (List<${entidad}>) results.values;
				notifyDataSetChanged();
			}
		}

	}
}
