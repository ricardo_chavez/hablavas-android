package ${basePackage};

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

import ${basePackage}.adapter.Listado${entidad}Adapter;
import ${basePackage}.model.GestorPrincipal;
import ${basePackage}.model.bean.${entidad};


public class Listado${entidad}FragmentActivity extends FragmentActivity implements
		OnItemClickListener, TextWatcher, OnClickListener {
	private ListView lviListado;
	private Listado${entidad}Adapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_listado_${entidadEnResources});
		lviListado = (ListView) findViewById(R.id.lviListado${entidad});
		adapter = new Listado${entidad}Adapter(this, 
			GestorPrincipal.getInstance().obtenerListado${entidad}(getApplicationContext()));
		if (adapter.getCount() == 0) {
			(findViewById(R.id.tviEmpty)).setVisibility(View.VISIBLE);
		}
		lviListado.setAdapter(adapter);
		lviListado.setOnItemClickListener(this);

		((EditText) findViewById(R.id.eteBuscar)).addTextChangedListener(this);
		//findViewById(R.id.butMenuInicio).setOnClickListener(this);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	public void onItemClick(AdapterView<?> adapter, View view, int position,long id) {
		${entidad} entidad_seleccionada = (${entidad})adapter.getItemAtPosition(position);
		GestorPrincipal.getInstance().setSeleccion${entidad}(entidad_seleccionada);
		//startActivity(new Intent(this,.class));
	}

	public void afterTextChanged(Editable s) {
		adapter.getFilter().filter(s.toString());
	}

	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	@Override
	public void onClick(View view) {
		@SuppressWarnings("rawtypes")
		Class siguienteFragmentActivity = null;
		/*switch (view.getId()) {
		case R.id.ibuVerMapa:
			siguienteFragmentActivity = ExpositoresMapaFragmentActivity.class;
			break;
		case R.id.butMenuInicio:
			finish();
			return;
		}
		startActivity(new Intent(this,siguienteFragmentActivity));*/
	}

}
