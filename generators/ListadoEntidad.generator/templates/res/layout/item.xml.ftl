<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="@dimen/height_item_listado"
    android:gravity="center_vertical"
    android:padding="5dip"
    >

    <ImageView
        android:id="@+id/iviImagen"
        android:layout_width="wrap_content"
        android:layout_height="match_parent"
        android:layout_gravity="center_vertical"
        android:adjustViewBounds="true"
        android:contentDescription="@string/empty_description"
        android:scaleType="fitCenter"/>

    <TextView
        android:id="@+id/tviTitulo"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:gravity="right|center_vertical"
        />

</LinearLayout>