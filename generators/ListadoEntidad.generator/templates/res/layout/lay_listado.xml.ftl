<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical" >

    <EditText
        android:id="@+id/eteBuscar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:inputType="text"/>

    <ListView
        android:id="@+id/lviListado${entidad}"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical" >

    </ListView>

    <TextView
        android:id="@+id/tviEmpty"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:text="@string/sin_datos_listado_${entidadEnResources}"
        android:visibility="gone" />

</LinearLayout>