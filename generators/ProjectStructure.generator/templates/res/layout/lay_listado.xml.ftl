<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="fill_parent"
    android:layout_height="fill_parent"
    android:orientation="vertical" 
    android:background="@drawable/layer_list_fondo_principal">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="@dimen/height_search"
        android:orientation="horizontal" 
        android:background="@android:color/transparent">

        <LinearLayout
            android:layout_width="0dip"
            android:layout_height="match_parent"
            android:paddingLeft="@dimen/height_search_inner_padding_left" 
            android:paddingRight="0dp"
            android:paddingBottom="@dimen/height_search_inner_padding_bottom" 
            android:background="@drawable/bg_elemento" 
            android:gravity="center_vertical"
            android:layout_weight="1">

            <EditText
                android:id="@+id/eteBuscar"
                android:layout_width="0dip"
                android:layout_height="@dimen/height_search_inner"
                android:paddingLeft="5dp"
                android:paddingRight="5dp"
                android:paddingTop="0dp"
                android:paddingBottom="0dp"
                android:background="@drawable/bg_search_inner"
                android:inputType="text"
                android:layout_weight="1"
                />
            <ImageView 
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:src="@drawable/ic_search"
                android:contentDescription="@string/empty_description"/>
        </LinearLayout>
		<ImageButton 
                android:id="@+id/ibuVerMapa"
                android:layout_width="57dp"
                android:layout_height="match_parent"
                android:src="@drawable/ic_mapa"
                android:background="@drawable/selector_menus_bg"
                android:contentDescription="@string/ver_mapa"/>
        <Button
            android:id="@+id/butMenuInicio"
            android:layout_width="wrap_content"
            android:layout_height="match_parent"
            android:layout_gravity="right"
            android:background="@drawable/selector_menus_bg"
            android:paddingBottom="@dimen/padding_bottom_menu"
            android:text="@string/menu_inicio"
            android:textColor="@drawable/selector_menus_font"
            android:textSize="12sp" />
    </LinearLayout>

    <ListView
        android:id="@+id/lviExpositores"
        android:layout_width="match_parent"
        android:layout_height="0dip"
        android:layout_weight="5"
        android:orientation="vertical" >

    </ListView>

    <TextView
        android:id="@+id/tviEmpty"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:text="@string/sin_datos_expositores"
        android:visibility="gone" />

    <include layout="@layout/merge_footer"/>

</LinearLayout>