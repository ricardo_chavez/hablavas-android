<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="fill_parent"
    android:layout_height="fill_parent"
    android:layout_gravity="center"
    android:orientation="vertical" >

    <LinearLayout 
        android:layout_width="match_parent"
        android:layout_height="@dimen/height_search"
        android:orientation="horizontal">

        <Button
            android:id="@+id/butMenu1"
            android:layout_width="wrap_content"
            android:layout_height="match_parent"
            android:background="@drawable/selector_menus_bg"
            android:paddingBottom="@dimen/padding_bottom_menu"
            android:text="@string/contacto"
            android:textColor="@drawable/selector_menus_font"
            android:textSize="12sp" />
        <View android:layout_width="0dip" android:layout_height="0dip"
            android:layout_weight="1"/>
        <Button
            android:id="@+id/butMenuExpositores"
            android:layout_width="wrap_content"
            android:layout_height="match_parent"
            android:background="@drawable/selector_menus_bg"
            android:paddingBottom="@dimen/padding_bottom_menu"
            android:text="@string/menu_expositores"
            android:textColor="@drawable/selector_menus_font"
            android:textSize="12sp" />
        <View android:layout_width="0dip" android:layout_height="0dip"
            android:layout_weight="0"/>
        <Button
            android:id="@+id/butMenuInicio"
            android:layout_width="wrap_content"
            android:layout_height="match_parent"
            android:background="@drawable/selector_menus_bg"
            android:paddingBottom="@dimen/padding_bottom_menu"
            android:text="@string/menu_inicio"
            android:textColor="@drawable/selector_menus_font"
            android:textSize="12sp" />
    </LinearLayout>
    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent" 
        android:padding="@dimen/padding_texto">

        <LinearLayout
            android:layout_width="fill_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical" >

            <TextView
                android:id="@+id/tviExpositor"
                style="@style/HeaderTextView"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_vertical|center_horizontal|center"
                android:textSize="20sp" />

            <ImageView
                android:id="@+id/ivi_Expositor"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_gravity="center"
                android:adjustViewBounds="true"
                android:contentDescription="@string/empty_description"
                android:maxHeight="200dp"
                android:padding="25dp"
                android:scaleType="centerInside"
                android:src="@drawable/default_image" />

            <TextView
                style="@style/HeaderTextView"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="@string/descripcion"
                android:textSize="16sp" />

            <TextView
                android:id="@+id/tviDesripcion"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:textSize="16sp"
                android:textIsSelectable="true" />

            <TextView
                style="@style/HeaderTextView"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="@string/contactenos"
                android:textSize="16sp" />

            <TextView
                android:id="@+id/tviContactenos"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:textSize="16sp" 
                android:textIsSelectable="true"/>

        </LinearLayout>
    </ScrollView>

    <include layout="@layout/merge_footer"/>

</LinearLayout>