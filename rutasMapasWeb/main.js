var rendererOptions = {
  draggable: true
};
var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);;
var directionsService = new google.maps.DirectionsService();
var map;

var punto_inicial = new google.maps.LatLng(-12.0505,-77.0505);
var zoom_inicial = 12;
var puntos = "";
function initialize() {
  var mapOptions = {
    zoom: zoom_inicial,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    center: punto_inicial
  };
  map = new google.maps.Map($("#map_canvas")[0], mapOptions);
  directionsDisplay.setMap(map);
  google.maps.event.addListener(directionsDisplay, 'directions_changed', function() {
    computeTotalDistance(directionsDisplay.directions);
  });

  calcRoute();
}

function calcRoute() {
  var start = $("#start").val();
  var end = $("#end").val();

  var request = {
    origin: start,
    destination: end,
    waypoints:[],
    travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}  
function saveRoute(){
	var nombre = $("#nombre").val();
	if(!nombre.replace(/\s/g,"") || 0 === nombre.replace(/\s/g,"").length){
		alert("Ingrese un nombre.");
	}else if(puntos.length === 0){
		alert("Describa una ruta.");
	}else{
		$("#loading").show();
		$.ajax({
			type: "POST",
			url: "servicioRutaMapas.php",
			data: { "nombre": nombre, "puntos": puntos }
			}).done(function( msg ) {
				alert(msg);
				$("#loading").hide();
			});
	}
}
function computeTotalDistance(result) {
  puntos = "";
  var myroute = result.routes[0];
  for (i = 0; i < myroute.overview_path.length; i++) {
	  puntos += myroute.overview_path[i].lat() + "," 
	  			+ myroute.overview_path[i].lng() + ",";
	  //toString() 	usa (lat,lng)
  }
  puntos = puntos.substring(0, puntos.length - 2 );
}
 