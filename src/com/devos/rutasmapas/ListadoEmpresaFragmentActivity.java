package com.devos.rutasmapas;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.devos.rutasmapas.adapter.ListadoEmpresaAdapter;
import com.devos.rutasmapas.model.GestorPrincipal;


public class ListadoEmpresaFragmentActivity extends FragmentActivity implements
		TextWatcher, OnClickListener {
	private ListView lviListado;
	private ListadoEmpresaAdapter adapter;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_listado_empresa);
		lviListado = (ListView) findViewById(R.id.lviListadoEmpresa);
		adapter = new ListadoEmpresaAdapter(this, 
			GestorPrincipal.getInstance().getListadoEmpresa(getApplicationContext()));
		if (adapter.getCount() == 0) {
			(findViewById(R.id.tviEmpty)).setVisibility(View.VISIBLE);
		}
		lviListado.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		lviListado.setAdapter(adapter);
		for (int i = 0; i < lviListado.getCount(); i++) { lviListado.setItemChecked(i, false); }
		((EditText) findViewById(R.id.eteBuscar)).addTextChangedListener(this);
		findViewById(R.id.iviHeaderBack).setOnClickListener(this);
		findViewById(R.id.ibuHeaderNext).setOnClickListener(this);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
	
	public void afterTextChanged(Editable s) {
		adapter.getFilter().filter(s.toString());
	}

	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	@Override
	public void onClick(View view) {
		@SuppressWarnings("rawtypes")
		Class siguienteFragmentActivity = null;
		switch (view.getId()) {
		case R.id.iviHeaderBack:
			finish();
			return;
		case R.id.ibuHeaderNext:
			//lviListado.getCheckedItemIds() does not work because 
	        //LinearLayout doesn't implement Checkable
			List<Long> checkedItemIds = new ArrayList<Long>();
			for (int i = 0; i < adapter.getCount(); i++) {
				if(adapter.getItem(i).isSelected()){
					checkedItemIds.add(adapter.getItem(i).getId());
				}
			}
			if(checkedItemIds.size() == 0){
				Toast.makeText(this, getString(R.string.seleccione_al_menos_una)
						, Toast.LENGTH_SHORT).show();
				return;
			}
			GestorPrincipal.getInstance().setSelectedEmpresas(checkedItemIds);
			siguienteFragmentActivity = ListadoRutaFragmentActivity.class;
			break;
		}
		startActivity(new Intent(this,siguienteFragmentActivity));
	}
}
