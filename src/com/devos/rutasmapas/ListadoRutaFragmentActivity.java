package com.devos.rutasmapas;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.devos.android.DvTextView;
import com.devos.rutasmapas.adapter.ListadoRutaAdapter;
import com.devos.rutasmapas.model.GestorPrincipal;
import com.devos.rutasmapas.model.bean.Ruta;
import com.google.analytics.tracking.android.EasyTracker;


public class ListadoRutaFragmentActivity extends FragmentActivity implements
		TextWatcher, OnClickListener {
	private ListView lviListado;
	private ListadoRutaAdapter adapter;
	private DvTextView txtTitulo;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_listado_ruta);
		lviListado = (ListView) findViewById(R.id.lviListadoRuta);
		
		adapter = new ListadoRutaAdapter(this, 
				GestorPrincipal.getInstance().getListadoRutasBySelectedOption(this));
		if (adapter.getCount() == 0) {
			(findViewById(R.id.tviEmpty)).setVisibility(View.VISIBLE);
		}
		lviListado.setAdapter(adapter);

		txtTitulo = (DvTextView) findViewById(R.id.tviHeader);
		txtTitulo.setText(getString(R.string.rutas));
		
		((EditText) findViewById(R.id.eteBuscar)).addTextChangedListener(this);
		((ImageButton)findViewById(R.id.ibuHeaderNext))
			.setImageResource(R.drawable.selector_ver_mapa);
		findViewById(R.id.iviHeaderLogo).setVisibility(View.GONE);
		findViewById(R.id.iviHeaderBack).setOnClickListener(this);
		findViewById(R.id.ibuHeaderNext).setOnClickListener(this);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); 
	}
    @Override
    public void onStop() {
      super.onStop();
      EasyTracker.getInstance().activityStop(this); 
    }
	public void afterTextChanged(Editable s) {
		adapter.getFilter().filter(s.toString());
	}

	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	@Override
	public void onClick(View view) {
		@SuppressWarnings("rawtypes")
		Class siguienteFragmentActivity = null;
		switch (view.getId()) {
		case R.id.iviHeaderBack:
		case R.id.iviHeaderLogo:
			finish();
			return;
		case R.id.ibuHeaderNext:
			List<Ruta> checkedItemObjects = new ArrayList<Ruta>();
			for (int i = 0; i < adapter.getCount(); i++) {
				if(adapter.getItem(i).isSelected()){
					checkedItemObjects.add(adapter.getItem(i));
				}
			}
			if(checkedItemObjects.size() != 0){
				GestorPrincipal.getInstance().setSelectedRutas(checkedItemObjects);
			}else{
				GestorPrincipal.getInstance().setSelectedRutas(null);
			}
			siguienteFragmentActivity = MapaFragmentActivity.class;
			break;
		}
		startActivity(new Intent(this,siguienteFragmentActivity));
	}

}
