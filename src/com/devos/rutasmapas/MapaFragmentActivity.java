package com.devos.rutasmapas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.devos.android.DvTextView;
import com.devos.android.utils.Utils;
import com.devos.rutasmapas.model.GestorPrincipal;
import com.devos.rutasmapas.model.bean.Punto;
import com.devos.rutasmapas.model.bean.Ruta;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.LatLngBounds.Builder;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapaFragmentActivity extends android.support.v4.app.FragmentActivity 
implements OnClickListener, OnCameraChangeListener, OnMarkerClickListener {
    private static int WIDTH = 3;
    private GoogleMap mMap;
	private DvTextView txtTitulo;
	private int mMapBoundsWidth;
	private int mMapBoundsHeight;
	private int mMapBoundsPadding;
	private float MAP_BOUNDS_PADDING_DP = 35.0f;
	private float VALUE_FOR_ROUND = 0.5f;
	private Builder mMapBoundsBuilder;
	float zoom = GestorPrincipal.MAP_ZOOM_MAP;
	public static final float MAX_ZOOM = 16f;
	int duration = GestorPrincipal.MAP_ANIMATION;
	boolean automaticUpdate = true;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_mapa);
        setUpMapIfNeeded();
		
		txtTitulo = (DvTextView) findViewById(R.id.tviHeader);
		txtTitulo.setText(getString(R.string.mapa));
		
		findViewById(R.id.iviHeaderLogo).setVisibility(View.GONE);
		findViewById(R.id.iviHeaderBack).setOnClickListener(this);
		findViewById(R.id.ibuHeaderNext).setVisibility(View.GONE);
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(GestorPrincipal.MAP_DEFAULT_LOCATION,
				GestorPrincipal.MAP_DEFAULT_ZOOM));
		mMap.setOnCameraChangeListener(this);
		
		WIDTH = getResources().getDimensionPixelSize(R.dimen.width_for_routes_in_pixels);
		//calculating bounds
		DisplayMetrics metrics = getResources().getDisplayMetrics();
    	final float scale = getResources().getDisplayMetrics().density;
    	int height_header = getResources().getDimensionPixelSize(R.dimen.height_header);
    	mMapBoundsWidth = metrics.widthPixels;
    	mMapBoundsHeight = metrics.heightPixels - height_header;
    	mMapBoundsPadding = (int) (MAP_BOUNDS_PADDING_DP * scale + VALUE_FOR_ROUND);
        
    	mMapBoundsBuilder = LatLngBounds.builder();
    	//drawing Routes
    	List<Ruta> rutas = GestorPrincipal.getInstance().getSelectedRutas();
    	if(rutas != null){
    		//TODO use fragment. Avoid losing the routes if activity is restarted
    		for (int i = 0; i < rutas.size(); i++) {
    			pintarMapa(rutas.get(i).getPuntoList(),rutas.get(i).getColor());
    		}
    		zoomingToRoutes();
    	}
    	if(GestorPrincipal.getInstance().getSelectedOption() 
    			== SearchFragmentActivity.OPTIONS_FILTERED_ROUTES){
    		drawVisibleRoutesZone();
    	}
        /*//or use onLocationChanged
        UIUpdater mUIUpdater = new UIUpdater(new Runnable() {
            @Override 
            public void run() {
            }
       },GestorPrincipal.MIN_TIME_BW_UPDATES);
	   mUIUpdater.startUpdates();*/
    	
    }
    @Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); 
	}
    @Override
    public void onStop() {
      super.onStop();
      EasyTracker.getInstance().activityStop(this);
    }

	private void setUpMapIfNeeded() {
		//warn the user about internet
		if (!Utils.isOnline(this)) {
			Toast.makeText(this,getString(R.string.without_internet_conection), Toast.LENGTH_LONG)
			.show();
		}
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
            	mMap.setMyLocationEnabled(true);
            }
        }
    }
	private void drawVisibleRoutesZone(){
		CircleOptions circleOptions;
		if(GestorPrincipal.getInstance().getLocationOrigin() != null){
			circleOptions = new CircleOptions()
			.center(GestorPrincipal.getInstance().getLocationOrigin())
			.radius(GestorPrincipal.getInstance().getVisibleRoutesZoneRadiusFromOrigin())
			.strokeColor(android.R.color.transparent)
			.fillColor(getResources().getColor(R.color.azul_claro_semi_transparent));
			mMap.addCircle(circleOptions);
			
			mMap.addMarker(new MarkerOptions()
	    	.position(GestorPrincipal.getInstance().getLocationOrigin())
	    	.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_origen)));
		}
		if(GestorPrincipal.getInstance().getLocationDestination() != null){
			circleOptions = new CircleOptions()
			.center(GestorPrincipal.getInstance().getLocationDestination())
			.radius(GestorPrincipal.getInstance().getVisibleRoutesZoneRadiusFromDestination())
			.strokeColor(android.R.color.transparent)
			.fillColor(getResources().getColor(R.color.azul_claro_semi_transparent)); 
			mMap.addCircle(circleOptions);
			
			mMap.addMarker(new MarkerOptions()
			.position(GestorPrincipal.getInstance().getLocationDestination())
			.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destino)));
		}
		mMap.setOnMarkerClickListener(this);
	}
	private void zoomingToRoutes() {
		List<CameraUpdate> updates = new ArrayList<CameraUpdate>();
		LatLngBounds bounds = mMapBoundsBuilder.build();
		updates.add(CameraUpdateFactory.newLatLngBounds(bounds, 
				mMapBoundsWidth, mMapBoundsHeight, mMapBoundsPadding));
		//not zooming this if all routees options has been selected
		if(GestorPrincipal.getInstance().getSelectedOption()
				== SearchFragmentActivity.OPTIONS_FILTERED_ROUTES){
			if(GestorPrincipal.getInstance().getMyLocation() != null){
				updates.add(CameraUpdateFactory.newLatLngZoom(
						GestorPrincipal.getInstance().getMyLocation(),zoom));
			}
			mMapBoundsBuilder = LatLngBounds.builder();
			//this two points validate LatLngBounds (number of points > 2).
			mMapBoundsBuilder.include(GestorPrincipal.getInstance().getLocationOrigin());
			mMapBoundsBuilder.include(GestorPrincipal.getInstance().getLocationDestination());
			bounds = mMapBoundsBuilder.build();
			updates.add(CameraUpdateFactory.newLatLngBounds(bounds, 
					mMapBoundsWidth, mMapBoundsHeight, mMapBoundsPadding));
		}
    	loopAnimateCamera(updates);
	}

	//from https://github.com/mg6maciej/android-maps-v2-demo/blob/master/NewMapsDemo/src/pl/mg6/newmaps/demo/AnimateCameraChainingExampleActivity.java#L58
	private void loopAnimateCamera(final List<CameraUpdate> updates) {
		if(updates.size() > 0){
			automaticUpdate = true;
			CameraUpdate update = updates.remove(0);
			//updates.add(update);
			mMap.animateCamera(update, duration, new CancelableCallback() {
				
				@Override
				public void onFinish() {
					loopAnimateCamera(updates);
				}
				
				@Override
				public void onCancel() {
				}
			});
		}else{
			automaticUpdate = false;
		}
	}
    private void pintarMapa(List<Punto> listRoute,int color) {
    	PolylineOptions options = new PolylineOptions();
    	Punto punto;
    	LatLng latLng;
    	for (Iterator<Punto> iterator = listRoute.iterator(); iterator.hasNext();) {
    		punto = iterator.next();
    		latLng = new LatLng(punto.getLatitud(),punto.getLongitud());
    		options.add(latLng);
    		mMapBoundsBuilder.include(latLng);
    	}
    	mMap.addPolyline(options
    			.color(color)
    			.geodesic(true)
    			.width(WIDTH));
    }
    
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.iviHeaderBack:
		case R.id.iviHeaderLogo:
			//TODO improve. Reuse activity
			finish();
			return;
		}
	}
	@Override
	public void onCameraChange(CameraPosition camaraPosition) {
	    if (automaticUpdate && camaraPosition.zoom > MAX_ZOOM)
	        mMap.animateCamera(CameraUpdateFactory.zoomTo(MAX_ZOOM));
	}
	@Override
	public boolean onMarkerClick(Marker marker) {
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(),zoom));
		return true;
	}
}
