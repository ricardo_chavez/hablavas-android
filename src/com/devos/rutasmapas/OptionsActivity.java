package com.devos.rutasmapas;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.devos.android.DvTextView;
import com.devos.rutasmapas.model.GestorPrincipal;


public class OptionsActivity extends PreferenceActivity 
	implements OnPreferenceChangeListener, OnClickListener{
	private Preference prefOrigin;
	private Preference prefDestination;
	@SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //for compatibility 
        //http://stackoverflow.com/questions/10186697/preferenceactivity-android-4-0-and-earlier/10258323#10258323
        addPreferencesFromResource(R.xml.settings);
        setContentView(R.layout.preferences_layout);
        DvTextView txtTitulo = (DvTextView) findViewById(R.id.tviHeader);
		txtTitulo.setText(getString(R.string.options));
        findViewById(R.id.iviHeaderLogo).setVisibility(View.GONE);
		findViewById(R.id.iviHeaderBack).setOnClickListener(this);
		findViewById(R.id.ibuHeaderNext).setVisibility(View.GONE);
		
        prefOrigin = getPreferenceScreen()
        		.findPreference(GestorPrincipal.PREF_RADIUS_ORIGIN);
        prefOrigin.setOnPreferenceChangeListener(this);
        prefOrigin.setLayoutResource(R.layout.preference);
        Preference prefDestination = getPreferenceScreen()
        		.findPreference(GestorPrincipal.PREF_RADIUS_DESTINATION);
        prefDestination.setOnPreferenceChangeListener(this);
        prefDestination.setLayoutResource(R.layout.preference);
        
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		String new_radius_string_origin = sharedPrefs.
				getString(GestorPrincipal.PREF_RADIUS_ORIGIN,"");
		prefOrigin.setSummary(
				getString(R.string.pref_radius_visible_routes_zone, 
						new_radius_string_origin));
		String new_radius_string_destination = sharedPrefs.
				getString(GestorPrincipal.PREF_RADIUS_DESTINATION,"");
		prefDestination.setSummary(
				getString(R.string.pref_radius_visible_routes_zone, 
						new_radius_string_destination));
    }
	private boolean rangeCheck(Object newValue) {
		try{
			if(Integer.parseInt(String.valueOf(newValue)) < 
				GestorPrincipal.VISIBLE_ZONE_ROUTE_RADIUS_MIN
				|| Integer.parseInt(String.valueOf(newValue)) > 
				GestorPrincipal.VISIBLE_ZONE_ROUTE_RADIUS_MAX){
				Toast.makeText(this,
						getResources().getString(R.string.is_out_of_range,
								newValue,
								GestorPrincipal.VISIBLE_ZONE_ROUTE_RADIUS_MIN,
								GestorPrincipal.VISIBLE_ZONE_ROUTE_RADIUS_MAX),
								Toast.LENGTH_SHORT).show();
				return false;
			}
		}catch(NumberFormatException e){
			Toast.makeText(this,getResources().getString(R.string.invalid_value),
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}
	
	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		//Check that the string is an integer.
		if(rangeCheck(newValue)){
			preference.setSummary(getString(R.string.pref_radius_visible_routes_zone,newValue));
			return true;
		}
		return false;
	}
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.iviHeaderBack:
		case R.id.iviHeaderLogo:
			//TODO improve. Reuse activity
			finish();
			return;
		}
	}
}
