package com.devos.rutasmapas;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.devos.android.DvTextView;
import com.devos.rutasmapas.model.GestorPrincipal;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class PointInMapSelectionFragmentActivity extends android.support.v4.app.FragmentActivity 
	implements OnClickListener, OnMapClickListener, OnInfoWindowClickListener {
    private GoogleMap mMap;
	private DvTextView txtTitulo;
	float zoom = GestorPrincipal.MAP_ZOOM_POINT_IN_MAP;
	int duration = GestorPrincipal.MAP_ANIMATION;
	private int idOption; 
	private LatLng pointInMap;
	private LatLng mLastSelectionPoint;
	private int mLastSelectionRadious;
	private Marker mSelectionMarker;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_mapa);
        setUpMapIfNeeded();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(GestorPrincipal.MAP_DEFAULT_LOCATION,
				GestorPrincipal.MAP_DEFAULT_ZOOM));
		txtTitulo = (DvTextView) findViewById(R.id.tviHeader);
		txtTitulo.setText(getString(R.string.to));
		
		findViewById(R.id.iviHeaderLogo).setVisibility(View.GONE);
		findViewById(R.id.iviHeaderBack).setOnClickListener(this);
		findViewById(R.id.ibuHeaderNext).setVisibility(View.GONE);
		
		idOption = getIntent().getIntExtra(SearchFragmentActivity.KEY_SPINNER, 0);
		String option = getString(R.string.destination);
		mLastSelectionPoint = GestorPrincipal.getInstance().getLocationDestination();
		mLastSelectionRadious = GestorPrincipal.getInstance().getVisibleRoutesZoneRadiusFromDestination();
		switch (idOption) {
		case R.string.from:
			option = getString(R.string.origin);
			txtTitulo.setText(getString(R.string.from));
			mLastSelectionPoint = GestorPrincipal.getInstance().getLocationOrigin();
			mLastSelectionRadious = GestorPrincipal.getInstance().getVisibleRoutesZoneRadiusFromOrigin();
			break;
		}
    	drawMap();
    	drawVisibleRoutesZone();
    	zoomingToRoutes();
    	
    	Toast.makeText(this, getString(R.string.tap_to_point_in_map, option), 
    			Toast.LENGTH_LONG).show();
    }
    @Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); 
	}
    @Override
    public void onStop() {
      super.onStop();
      EasyTracker.getInstance().activityStop(this); 
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
            	mMap.setMyLocationEnabled(true);
            }
        }
    }
    private void drawVisibleRoutesZone(){
		if(mLastSelectionPoint != null){
			CircleOptions circleOptions = new CircleOptions()
			.center(mLastSelectionPoint)
			.radius(mLastSelectionRadious)
			.strokeColor(android.R.color.transparent)
			.fillColor(getResources().getColor(R.color.azul_claro_semi_transparent));
			mMap.addCircle(circleOptions);
		}
	}
	private void zoomingToRoutes() {
		List<CameraUpdate> updates = new ArrayList<CameraUpdate>();
		if(GestorPrincipal.getInstance().getMyLocation() != null){
			updates.add(CameraUpdateFactory.newLatLngZoom(
					GestorPrincipal.getInstance().getMyLocation(),zoom));
    	}
		if(mLastSelectionPoint != null){
			updates.add(CameraUpdateFactory.newLatLngZoom(mLastSelectionPoint,zoom));
		}
    	loopAnimateCamera(updates);
	}
	
	//from https://github.com/mg6maciej/android-maps-v2-demo/blob/master/NewMapsDemo/src/pl/mg6/newmaps/demo/AnimateCameraChainingExampleActivity.java#L58
	private void loopAnimateCamera(final List<CameraUpdate> updates) {
		if(updates.size() > 0){
			CameraUpdate update = updates.remove(0);
			//updates.add(update);
			mMap.animateCamera(update, duration, new CancelableCallback() {
				@Override
				public void onFinish() {
					loopAnimateCamera(updates);
				}
				@Override
				public void onCancel() {
				}
			});
		}
	}
    private void drawMap() {
    	mMap.setOnMapClickListener(this);
    	mMap.setOnInfoWindowClickListener(this);
    }
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.iviHeaderBack:
		case R.id.iviHeaderLogo:
			finish();
			return;
		}
	}
	@Override
	public void onMapClick(LatLng latLng) {
		pointInMap = latLng;
		if(mSelectionMarker == null){
			mSelectionMarker = mMap.addMarker(
					new MarkerOptions()
					.position(latLng)
					.title(getString(R.string.tap_to_select_this_point))
					.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_transparent))
							);
		}
		mSelectionMarker.setPosition(latLng);
		mSelectionMarker.showInfoWindow();
	}
	@Override
	public void onInfoWindowClick(Marker arg0) {
		//selection done
		switch (idOption) {
		case R.string.from:
			GestorPrincipal.getInstance().setLocationOrigin(pointInMap);
			break;
		case R.string.to:
			GestorPrincipal.getInstance().setLocationDestination(pointInMap);
			break;
		}
		setResult(Activity.RESULT_OK);
		finish();
	}
}