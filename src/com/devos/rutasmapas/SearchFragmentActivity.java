package com.devos.rutasmapas;


import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.devos.rutasmapas.model.GestorPrincipal;
import com.devos.rutasmapas.model.GestorPrincipal.FilteringHandler;
import com.devos.rutasmapas.model.bean.Ruta;
import com.devos.rutasmapas.model.dao.GestorBaseDeDatos;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.maps.model.LatLng;


public class SearchFragmentActivity extends FragmentActivity 
	implements OnClickListener, FilteringHandler, LocationTrackerService.LocationChangedListener, 
	android.content.DialogInterface.OnClickListener{
	public static final int OPTIONS_NONE = -1;
	public static final int OPTIONS_MY_LOCATION = 0;
	public static final int OPTIONS_POINT_IN_MAP = 1;
	public static final int OPTIONS_ALL_ROUTES = 2;
	public static final int OPTIONS_FILTERED_ROUTES = 3;
	public static final int OPTIONS_TOTAL = 2;
	public static final String KEY_SPINNER = "key_spinner";
	public static final int REQUEST_CODE_OPTIONS = 2;
	public static final int REQUEST_CODE_POINT_IN_MAP = 0;
	private Button butFrom;
	private Button butTo;
	private ProgressDialog pdiFiltering;
	private boolean mIsWaitingForLocationAndDirectShowRoutes;
	Criteria myCriteria = new Criteria();
	boolean enabledOnly = false;
	boolean isGPSEnabled;
	private SharedPreferences sharedPrefs; 
	private LocationTrackerService locationTrackerService;
	private AlertDialog adiOptions;
	private AlertDialog.Builder builder;
	private int currentDialog;
	private int selectedOptionFrom = OPTIONS_MY_LOCATION;
	private int selectedOptionTo = OPTIONS_NONE;
	private String[] mArrayOptions;
	private boolean mIsFilteringChanged = true;
	private SearchFragmentActivity getInstance() {
		return this;
	}
	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); 
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_search);
		findViewById(R.id.butMostrar).setOnClickListener(this);
		findViewById(R.id.ibuOptions).setOnClickListener(this);
		findViewById(R.id.ibuShowAllRoutes).setOnClickListener(this);
		butFrom = (Button)findViewById(R.id.butFrom);
		butFrom.setOnClickListener(this);
		butTo = (Button)findViewById(R.id.butTo);
		butTo.setOnClickListener(this);
		pdiFiltering = new ProgressDialog(getInstance());
		pdiFiltering.setCanceledOnTouchOutside(false);
		pdiFiltering.setCancelable(false);
		pdiFiltering.setMessage(getString(R.string.selecting_nearest_routes));
		
		locationTrackerService = new LocationTrackerService(this, this);
		//to take advantage of the time
		locationTrackerService.getLocation();
		//TODO improvement. Consider when getting location takes too long.
		
		//setting colors
		List<Ruta> listRoutes = GestorPrincipal.getInstance().getListadoRutas(this);
		if(listRoutes.size() > 0){
			for (int j = 0; j < listRoutes.size(); j++) {
				listRoutes.get(j).setColor(GestorPrincipal.getInstance().getRandomColor());
			}
			GestorBaseDeDatos.getInstance().saveListRuta(this, listRoutes);
		}
		
		builder = new AlertDialog.Builder(this);
		setSelectedOptions();
	}
	@Override
	protected void onStop() {
		super.onStop();
		locationTrackerService.stopUsingGPS();
		EasyTracker.getInstance().activityStop(this); 
	}
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		//options are shown depending on selecting my location or point in map 
		// (from or to options)
		case R.id.butFrom:
			currentDialog = R.string.from;
			if(selectedOptionTo == OPTIONS_MY_LOCATION){
				showPointInMapSelectionDialog();
			}else{
				mArrayOptions = getResources().getStringArray(R.array.entries_options);
				builder.setItems(mArrayOptions, this);
				adiOptions = builder.create();
				adiOptions.setTitle(R.string.from);
				adiOptions.show();
			}
			break;
		case R.id.butTo:
			currentDialog = R.string.to;
			if(selectedOptionFrom == OPTIONS_MY_LOCATION){
				showPointInMapSelectionDialog();
			}else{
				mArrayOptions = getResources().getStringArray(R.array.entries_options);
				builder.setItems(mArrayOptions, this);
				adiOptions = builder.create();
				adiOptions.setTitle(R.string.to);
				adiOptions.show();			
			}
			break;
		case R.id.ibuShowAllRoutes:
			GestorPrincipal.getInstance().setSelectedOption(OPTIONS_ALL_ROUTES);
			gotoNextActivity();
			break;
		case R.id.ibuOptions:
			sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
			SharedPreferences.Editor editor = sharedPrefs.edit();
			editor.putString(GestorPrincipal.PREF_RADIUS_ORIGIN,
					String.valueOf(GestorPrincipal.getInstance()
							.getVisibleRoutesZoneRadiusFromOrigin()));
			editor.putString(GestorPrincipal.PREF_RADIUS_DESTINATION,
					String.valueOf(GestorPrincipal.getInstance()
							.getVisibleRoutesZoneRadiusFromDestination()));
			editor.commit();
			startActivityForResult(new Intent(this,OptionsActivity.class), REQUEST_CODE_OPTIONS);
			break;
		case R.id.butMostrar:
			if(selectedOptionTo == OPTIONS_NONE){
				Toast.makeText(this, 
						getString(R.string.to),
						Toast.LENGTH_SHORT).show();
				return;
			}
			//Logic to request location
			mIsWaitingForLocationAndDirectShowRoutes = false;
			locationTrackerService.getLocation();
			if (locationTrackerService.canGetLocation()) {
				//TODO suggest to enable gps
				if (GestorPrincipal.getInstance().getMyLocation() != null) {
					selectingNearestRoutes();
				} else {
					mIsWaitingForLocationAndDirectShowRoutes = true;
					Toast.makeText(this, 
							getString(R.string.wait_for_your_location_or_use_all_option),
							Toast.LENGTH_SHORT).show();
				}
			} else {
				//TODO disable my location if user do not have network or gps
				//Toast.makeText(this, 
				//		getString(R.string.no_location_service_available), Toast.LENGTH_LONG)
				//		.show();
				locationTrackerService.showSettingsAlert(this);
			}
			break;
		}
	}
	private void selectingNearestRoutes() {
		//set Location Origin when "from" is MyLocation
		if(selectedOptionFrom == OPTIONS_MY_LOCATION 
				&& GestorPrincipal.getInstance().getLocationOrigin() == null){
			GestorPrincipal.getInstance().setLocationOrigin(
					GestorPrincipal.getInstance().getMyLocation());
		}
		pdiFiltering.show();
		new Thread(new Runnable() {
		    public void run() {
		    	//avoid processing if user is using the same data for origin and destination 
				//	and proximity radius
		    	if(mIsFilteringChanged){
		    		GestorPrincipal.getInstance().getFilteredRoutesByLocation(
		    				getInstance(),getInstance());
		    	}else{
		    		onFinishFiltering();
		    	}
		    }
		}).start();
	}
	@Override
	public void onFinishFiltering() {
		pdiFiltering.dismiss();
		GestorPrincipal.getInstance().setSelectedOption(OPTIONS_FILTERED_ROUTES);
		gotoNextActivity();
	}
	@Override
	public void onLocationChanged() {
		if(locationTrackerService.getLocation() != null){
			GestorPrincipal.getInstance().setMyLocation(
					new LatLng(locationTrackerService.getLocation().getLatitude(),
							locationTrackerService.getLocation().getLongitude()
							));
			if(mIsWaitingForLocationAndDirectShowRoutes){
				mIsFilteringChanged = true;
				selectingNearestRoutes();
			}
		}
	}

	private void gotoNextActivity() {
		mIsWaitingForLocationAndDirectShowRoutes = false;
		mIsFilteringChanged = false;
		startActivity(new Intent(this,ListadoRutaFragmentActivity.class));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_CODE_OPTIONS:
			sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
			String new_radius_string_origin = sharedPrefs.
					getString(GestorPrincipal.PREF_RADIUS_ORIGIN,"");
			String new_radius_string_destination = sharedPrefs.
					getString(GestorPrincipal.PREF_RADIUS_DESTINATION,"");
			try{
				int new_radius_origin = Integer.parseInt(new_radius_string_origin);
				int new_radius_destination = Integer.parseInt(new_radius_string_destination);
				if(new_radius_origin != 0){
					GestorPrincipal.getInstance().
					setVisibleRoutesZoneRadiusFromOrigin(new_radius_origin);
				}
				if(new_radius_destination != 0){
					GestorPrincipal.getInstance().
					setVisibleRoutesZoneRadiusFromDestination(new_radius_destination);
				}
			}catch(NumberFormatException e){
				//just using the default value
			}
			break;
		case REQUEST_CODE_POINT_IN_MAP:
			if(resultCode == Activity.RESULT_OK){
				mIsFilteringChanged = true;
				switch (currentDialog) {
				case R.string.from:
					if(GestorPrincipal.getInstance().getLocationOrigin() != null){
						selectedOptionFrom = OPTIONS_POINT_IN_MAP; 
					}
					break;
				case R.string.to:
					if(GestorPrincipal.getInstance().getLocationDestination() != null){
						selectedOptionTo = OPTIONS_POINT_IN_MAP;
					}
					break;
				}
				setSelectedOptions();
			}
			break;
		}
	}
	@Override
	public void onClick(DialogInterface dialog, int which) {
		//total options help to distinguish between id = 0 for my location or id = 0 for point_in_map
		int totalOptions = ((AlertDialog)dialog).getListView().getAdapter().getCount();
		if(totalOptions == OPTIONS_TOTAL){
			if(which == OPTIONS_MY_LOCATION){
				if (GestorPrincipal.getInstance().getMyLocation() != null) {
					switch (currentDialog) {
					case R.string.from:
						GestorPrincipal.getInstance().setLocationOrigin(
								GestorPrincipal.getInstance().getMyLocation());
						selectedOptionFrom = OPTIONS_MY_LOCATION; 
						break;
					case R.string.to:
						GestorPrincipal.getInstance().setLocationDestination(
								GestorPrincipal.getInstance().getMyLocation());
						selectedOptionTo = OPTIONS_MY_LOCATION;
						break;
					}
					setSelectedOptions();
				}
			}else if(which == OPTIONS_POINT_IN_MAP){
				showPointInMapSelectionDialog();
			}
		}else{
			showPointInMapSelectionDialog();
		}
	}
	private void showPointInMapSelectionDialog(){
		Intent intent = new Intent(this,PointInMapSelectionFragmentActivity.class);
		intent.putExtra(KEY_SPINNER, currentDialog);
		startActivityForResult(intent, REQUEST_CODE_POINT_IN_MAP);
	}
	private void setSelectedOptions(){
		String[] options = getResources().getStringArray(R.array.entries_options);
		if(selectedOptionFrom >= 0 && selectedOptionFrom < options.length){
			butFrom.setText(getString(R.string.origin) + getString(R.string.separator) + 
					options[selectedOptionFrom]);
		}
		if(selectedOptionTo >= 0 && selectedOptionTo < options.length){
			butTo.setText(getString(R.string.destination) + getString(R.string.separator) +
					options[selectedOptionTo]);
		}else{
			butTo.setText(getString(R.string.to));
		}
	}
}