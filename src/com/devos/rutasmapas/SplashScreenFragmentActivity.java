package com.devos.rutasmapas;


import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;

import com.crittercism.app.Crittercism;
import com.devos.android.utils.Constantes;
import com.devos.android.utils.Utils;
import com.devos.rutasmapas.model.GestorPrincipal;
import com.devos.rutasmapas.model.dao.GestorBaseDeDatos;
import com.devos.rutasmapas.model.dao.GestorSincronizacion;
import com.devos.rutasmapas.model.dao.GestorSincronizacion.RespuestaSincronizacionHandler;

public class SplashScreenFragmentActivity extends FragmentActivity implements 
OnDismissListener,
GestorSincronizacion.ConectividadListener,RespuestaSincronizacionHandler, OnClickListener, OnCancelListener{
	private ProgressDialog pdiSincronizacion;
	protected Dialog mSplashDialog;
	public static int ESTADO_SPLASH_PRE_SINCRONIZACION = 0;
	public static int ESTADO_SPLASH_SINCRONIZANDO = 1;
	public static int ESTADO_SPLASH_POST_SINCRONIZACION = 2;//o post sincronizacion cancelada
	private int mEstadoSplashScreen;

	@SuppressWarnings("rawtypes")
	private Class nextFragmentActivity = SearchFragmentActivity.class;
	
	private SplashScreenFragmentActivity getInstance() {
		return this;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//CRITTERCISM CONFIGURATION BEGIN
		PackageManager manager = this.getPackageManager();
		String myCustomVersionName = "";
		try {
			PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
			myCustomVersionName = info.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		JSONObject crittercismConfig = new JSONObject();
		try {
			crittercismConfig.put("shouldCollectLogcat", true); 
			crittercismConfig.put("customVersionName", myCustomVersionName);
		} catch (JSONException je) {}
		Crittercism.init(getApplicationContext()
				, GestorPrincipal.CRITTERCISM_ID
				,crittercismConfig);
		//CRITTERCISM CONFIGURATION END
		
		setContentView(R.layout.lay_splash_screen);
		GestorBaseDeDatos.getInstance().inicializarDB(this.getApplicationContext());
		GestorBaseDeDatos.getInstance().setupConfiguracionInicial(this.getApplicationContext());
		
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		int currentDataVersion = sharedPrefs.getInt(GestorPrincipal.PREFS_DATA_VERSION, 0);
		
		boolean isLoadedDatabase = GestorBaseDeDatos.getInstance().getListEmpresa(this).size() > 0;
		if(isLoadedDatabase && currentDataVersion >= GestorPrincipal.DATA_VERSION){
			irHaciaActivity(nextFragmentActivity);
		}else{
			SharedPreferences.Editor editor = sharedPrefs.edit();
			editor.putInt(GestorPrincipal.PREFS_DATA_VERSION,GestorPrincipal.DATA_VERSION);
			editor.commit();
			GestorSincronizacion.getInstance().inicializarSincronizacion(this, this);
			iniciarSincronizacion();
		}
		
	}
	public void iniciarSincronizacion(){
		mEstadoSplashScreen = ESTADO_SPLASH_PRE_SINCRONIZACION;
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {
				if(mEstadoSplashScreen == ESTADO_SPLASH_PRE_SINCRONIZACION){
					pdiSincronizacion= new ProgressDialog(getInstance());
					pdiSincronizacion.setOnDismissListener(getInstance());
				    pdiSincronizacion.setCanceledOnTouchOutside(false); 
				    pdiSincronizacion.setMessage(getString(R.string.msg_sincronizacion));
					pdiSincronizacion.show();
					mEstadoSplashScreen = ESTADO_SPLASH_SINCRONIZANDO;
					GestorSincronizacion.getInstance().iniciarSincronizacionInterna(getInstance());
				}
			}
		}, GestorPrincipal.SPLASH_TIME_ANTES_DE_INICIAR_SINCRONIZACION);
	}
	
	private <T> void irHaciaActivity(Class<T> classe) {
		mEstadoSplashScreen = ESTADO_SPLASH_POST_SINCRONIZACION;
		Intent intent = new Intent(this, classe);
    	startActivity(intent);
    	finish();
	}

	public void finalizarSincronizacion() {
		//TODO validar que contexto no se pierda en SplashScreen 
		//android.view.WindowManager$BadTokenException: 
		//Unable to add window -- token android.os.BinderProxy@40517cf0 is not valid; 
		//is your activity running?
		mEstadoSplashScreen = ESTADO_SPLASH_POST_SINCRONIZACION;
		//It could be improve using ronOnUiThread on the RespuestaBaseDatosHandler
		runOnUiThread(new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				pdiSincronizacion.dismiss();
				if(GestorPrincipal.SPLASH_DIRECTO_A_MENU){
					irHaciaActivity(nextFragmentActivity);
				}else{
					new AlertDialog.Builder(getInstance())
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle(getString(R.string.app_tittle).toString())	
					.setMessage(R.string.msg_continuar)
					.setOnCancelListener(getInstance())
					.setNeutralButton(Constantes.confirmacion_continuar, getInstance())
					.show();
				}
			}
		});
	}
	
	public boolean isConectadoSinPaqueteDeDatos() {
		return false;
	}
	public boolean isConectado() {
	    return Utils.isOnline(this);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onClick(DialogInterface arg0, int arg1) {
		irHaciaActivity(nextFragmentActivity);
	}
	@SuppressWarnings("unchecked")
	public void onDismiss(DialogInterface dialog) {
		if(mEstadoSplashScreen != ESTADO_SPLASH_POST_SINCRONIZACION
				&& GestorPrincipal.SPLASH_SINCRONIZACION_CANCELABLE){
			if(GestorPrincipal.SPLASH_SINCRONIZACION_CANCELABLE_CON_CONFIRMACION){
				//TODO (optional) agregar confirmacion
			}else{
				irHaciaActivity(nextFragmentActivity);
			}
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onCancel(DialogInterface arg0) {
		irHaciaActivity(nextFragmentActivity);
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mEstadoSplashScreen = ESTADO_SPLASH_POST_SINCRONIZACION;
	}
}
