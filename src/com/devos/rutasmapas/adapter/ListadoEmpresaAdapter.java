package com.devos.rutasmapas.adapter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devos.rutasmapas.R;
import com.devos.rutasmapas.model.bean.Empresa;

public class ListadoEmpresaAdapter extends BaseAdapter implements Filterable {
	protected LayoutInflater mInflater;
	private List<Empresa> mListadoEmpresa;
	private List<Empresa> mListadoEmpresaInicial;
	private EmpresaFilter mEmpresaFilter;

	public ListadoEmpresaAdapter(Context context, List<Empresa> listadoEmpresa) {
		mInflater = LayoutInflater.from(context);
		mListadoEmpresa = mListadoEmpresaInicial = listadoEmpresa;
	}

	public int getCount() {
		return mListadoEmpresa.size();
	}

	public Empresa getItem(int position) {
		return mListadoEmpresa.get(position);
	}

	public long getItemId(int position) {
		return mListadoEmpresa.get(position).getId();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_empresa, null);
			viewHolder = new ViewHolder();
			viewHolder.llaContainer = (LinearLayout) convertView;
			viewHolder.tviTitulo = (TextView) convertView
					.findViewById(R.id.tviTitulo);
			viewHolder.ctvCheckbox = (CheckedTextView) convertView
					.findViewById(R.id.ctvCheckbox);
			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					CheckedTextView ctvCheckbox = ((CheckedTextView)view.
							findViewById(R.id.ctvCheckbox));
					//TODO improvement on memory
					//To use the viewholder and avoid problems when reusing item view.
					((Empresa)ctvCheckbox.getTag()).setSelected(!ctvCheckbox.isChecked());
					ctvCheckbox.toggle();
					//TODO fix issue with row selection
					//view.setSelected(ctvCheckbox.isChecked());
					 
				}
			});
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.llaContainer.setSelected(getItem(position).isSelected());
		viewHolder.ctvCheckbox.setTag(getItem(position));
		viewHolder.tviTitulo.setText(getItem(position).getAgenciaNombre());
		viewHolder.ctvCheckbox.setChecked(getItem(position).isSelected());
		return convertView;
	}
	public static class ViewHolder {
		LinearLayout llaContainer;
		TextView tviTitulo;
		CheckedTextView ctvCheckbox;
	}

	@Override
	public Filter getFilter() {
		if (mEmpresaFilter == null) {
			mEmpresaFilter = new EmpresaFilter();
		}
		return mEmpresaFilter;
	}

	private class EmpresaFilter extends Filter {
		@SuppressLint("DefaultLocale")
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults results = new FilterResults();
			if (constraint == null || constraint.length() == 0) {
				// No filter implemented we return all the list
				results.values = mListadoEmpresaInicial;
				results.count = mListadoEmpresaInicial.size();
			} else {
				// We perform filtering operation
				List<Empresa> nListadoEmpresa = new ArrayList<Empresa>();

				for (Empresa e : mListadoEmpresaInicial) {
					if (e.getAgenciaNombre().toUpperCase()
							.startsWith(constraint.toString().toUpperCase())) {
						nListadoEmpresa.add(e);
					}
				}
				results.values = nListadoEmpresa;
				results.count = nListadoEmpresa.size();
			}
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			if (results.count == 0)
				notifyDataSetInvalidated();
			else {
				mListadoEmpresa = (List<Empresa>) results.values;
				notifyDataSetChanged();
			}
		}

	}
}
