package com.devos.rutasmapas.adapter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.devos.rutasmapas.R;
import com.devos.rutasmapas.model.bean.Ruta;

public class ListadoRutaAdapter extends BaseAdapter implements Filterable {
	protected LayoutInflater mInflater;
	private List<Ruta> mListadoRuta;
	private List<Ruta> mListadoRutaInicial;
	private RutaFilter mRutaFilter;

	public ListadoRutaAdapter(Context context, List<Ruta> listadoRuta) {
		mInflater = LayoutInflater.from(context);
		mListadoRuta = mListadoRutaInicial = listadoRuta;
	}

	public int getCount() {
		return mListadoRuta.size();
	}

	public Ruta getItem(int position) {
		return mListadoRuta.get(position);
	}

	public long getItemId(int position) {
		return 0;
	}
	final String SEPARATOR = " - "; 
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_ruta, null);
			viewHolder = new ViewHolder();
			viewHolder.tviTitulo = (TextView) convertView
					.findViewById(R.id.tviTitulo);
			viewHolder.ctvCheckbox = (CheckedTextView) convertView
					.findViewById(R.id.ctvCheckbox);
			viewHolder.vieColor = convertView.findViewById(R.id.vieColor);
			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					CheckedTextView ctvCheckbox = ((CheckedTextView)view.
							findViewById(R.id.ctvCheckbox));
					//TODO improvement on memory
					//To use the viewholder and avoid problems when reusing item view.
					((Ruta)ctvCheckbox.getTag()).setSelected(!ctvCheckbox.isChecked());
					ctvCheckbox.toggle();
				}
			});
			viewHolder.ctvCheckbox.setTag(getItem(position));
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
			viewHolder.ctvCheckbox.setTag(getItem(position));
		}
		viewHolder.tviTitulo.setText(getTextItem(getItem(position)));
		viewHolder.ctvCheckbox.setChecked(getItem(position).isSelected());
		viewHolder.vieColor.setBackgroundColor(getItem(position).getColor());
		
		return convertView;
	}

	public static class ViewHolder {
		TextView tviTitulo;
		CheckedTextView ctvCheckbox;
		View vieColor;
	}

	@Override
	public Filter getFilter() {
		if (mRutaFilter == null) {
			mRutaFilter = new RutaFilter();
		}
		return mRutaFilter;
	}
	private String getTextItem(Ruta e){
		StringBuilder text = new StringBuilder();
		text.append(e.getEmpresa().getAgenciaNombre());
		text.append(SEPARATOR); 
		text.append(e.getRutaNombre());
		return text.toString();
	}
	
	private class RutaFilter extends Filter {
		@SuppressLint("DefaultLocale")
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults results = new FilterResults();
			if (constraint == null || constraint.length() == 0) {
				// No filter implemented we return all the list
				results.values = mListadoRutaInicial;
				results.count = mListadoRutaInicial.size();
			} else {
				// We perform filtering operation
				List<Ruta> nListadoRuta = new ArrayList<Ruta>();
				for (Ruta e : mListadoRutaInicial) {
					//Filter by all item text or route name or the number of the route
					if (getTextItem(e).toUpperCase()
							.contains(constraint.toString().toUpperCase())) {
						nListadoRuta.add(e);
					}
				}
				results.values = nListadoRuta;
				results.count = nListadoRuta.size();
			}
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			/*if (results.count == 0)
				notifyDataSetInvalidated();
			else {
				
			}*/
			mListadoRuta = (List<Ruta>) results.values;
			notifyDataSetChanged();
		}

	}
}
