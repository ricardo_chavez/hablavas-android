package com.devos.rutasmapas.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.location.Location;

import com.devos.rutasmapas.SearchFragmentActivity;
import com.devos.rutasmapas.model.bean.Configuracion;
import com.devos.rutasmapas.model.bean.Empresa;
import com.devos.rutasmapas.model.bean.Punto;
import com.devos.rutasmapas.model.bean.Ruta;
import com.devos.rutasmapas.model.dao.GestorBaseDeDatos;
import com.google.android.gms.maps.model.LatLng;

public class GestorPrincipal {
	private static final GestorPrincipal gestor = new GestorPrincipal();
	
	public static final String CRITTERCISM_ID = "51c1eb0e97c8f22cd4000002";
	//CONFIGURACION INICIAL
	public static final String CONFIG_DEFAULT_HOST = "desa1.devosinc.com";
	public static final String CONFIG_DEFAULT_PORT = null;
	public static final String CONFIG_DEFAULT_FECHA = "25/03/2013 19:00:00";
	public static final String PREFS_NAME = "Preferencias";
	public static final String PREFS_CONFIGURACION_ESTABLECIDA = "configuracion_establecida";
	public static final String PREFS_BASEDATOS_INICIAL_ESTABLECIDA = "basedatos_inicial_establecida";
	public static final String PREFS_DATA_VERSION = "data_version";
	public static final String PREF_RADIUS_ORIGIN = "prefRadiusOrigin";
	public static final String PREF_RADIUS_DESTINATION = "prefRadiusDestination";
	
	//DATA
	public static final String DATA_FILENAME = "json/ObtenerInformacion3.json";
	public static final int DATA_VERSION = 8;
	
	//SINCRONIZACION
	public static final boolean SINCRONIZACION_INICIAL = true;
	public static boolean SINCRONIZACION_PROCESO = false;//sincronizacion 1 a 1.
	
	//SPLASH SCREEN	
	public static final int SPLASH_TIME_ANTES_DE_INICIAR_SINCRONIZACION = 2000;
	public static final boolean SPLASH_SINCRONIZACION_CANCELABLE = true;
	public static final boolean SPLASH_SINCRONIZACION_CANCELABLE_CON_CONFIRMACION = false;
	public static final boolean SPLASH_DIRECTO_A_MENU = true;
	
	//SERVICIOS
	public static final String SERVICE_BASEPATH = "http://desa1.devosinc.com";
	public static final String SERVICE_APP ="/ServiciosJson/RutasMapas";
	public static final String SERVICE_API_KEY = "44dd798bae61b1e1f4a2c8cee559c18e";
	public static final String SERVICE_CLIENT_CONTENT_TYPE = "application/json";
	public static final String SERVICE_CLIENT_CHARSET = "UTF-8";
	public static final int SERVICE_CLIENT_TIMEOUT = 2000;
	public static final String SERVICE_REQUEST_BODY = "{'_key':'%s','fechaActualizacion': '%s'}";
	
	public static final int SERVICE_REQUESTS = 1;
	public static final String SERVICE_REQUEST_URL_EMPRESAS = "/ObtenerInformacion.json";

	//BASE DE DATOS
	//TODO Do not hardcode "/data/"; use Context.getFilesDir().getPath() instead
	@SuppressLint("SdCardPath")
	public static final String DB_PATH = "/data/data/com.devos.rutasmapas/databases/";
	public static final String DB_NAME_FROM_ASSETS = null;//"ProjectInitialDB.db";
	public static final String DB_NAME = "RutasMapas";

	//IMAGENES
	//TODO Do not hardcode "/data/"; use Context.getFilesDir().getPath() instead
	@SuppressLint("SdCardPath")
	public static final String IMAGES_PATH = "/data/data/com.devos.rutasmapas/app_imagenes/empresas";
	public static final String IMAGES_IMAGE_EXTENSION = ".png";
	
	//OTROS
	public static final String FORMAT_FECHA_ENVIO = "dd/MM/yyyy HH:mm:ss";

	//CONTENIDOS
	public static final boolean GPS_MY_LOCATION_ENABLED = true;
	public static final float GPS_ZOOM = 15f;
	public static final String PATH_ASSETS = "file:///android_asset/";

	public static final int MIN_TIME_BW_UPDATES = 60000;

	public static final float MAP_ZOOM_POINT_IN_MAP = 12.6f;
	public static final float MAP_ZOOM_MAP = 14.6f;
	public static final int MAP_ANIMATION = 2000;
	public static final LatLng MAP_DEFAULT_LOCATION = new LatLng(-12.054671161166516, -77.03960325568914);
	public static final float MAP_DEFAULT_ZOOM = 10.785426f;
	public static final int VISIBLE_ZONE_ROUTE_RADIUS_MIN = 100;
	public static final int VISIBLE_ZONE_ROUTE_RADIUS_MAX = 2000;

	private Configuracion configuracion;
	private int visibleRoutesZoneRadiusFromOrigin = 600;//100m
	private int visibleRoutesZoneRadiusFromDestination = 600;//100m
	private LatLng myLocation;
	private LatLng locationOrigin;
	private LatLng locationDestination;

	private List<Long> selectedEmpresaIds;
	private List<Ruta> selectedRutas;
	private List<Ruta> listFilteredRoutes = new ArrayList<Ruta>();
	private int selectedAllOption;

	public int getSelectedOption() {
		return selectedAllOption;
	}
	public void setSelectedOption(int selectedAllOption) {
		this.selectedAllOption = selectedAllOption;
	}
	public static GestorPrincipal getInstance(){
		return gestor;
	}
	public void setConfiguracion(Configuracion configuracion) {
		this.configuracion = configuracion;
	}
	public Configuracion getConfiguracion() {
		return configuracion;
	}
	public List<Empresa> getListadoEmpresa(Context context) {
		return GestorBaseDeDatos.getInstance().getListEmpresa(context);
	}
	public List<Ruta> getListadoRutasBySelectedOption(Context context) {
		//return GestorBaseDeDatos.getInstance().getRutas(context, selectedEmpresaIds);
		if(selectedAllOption == SearchFragmentActivity.OPTIONS_ALL_ROUTES){
			return GestorBaseDeDatos.getInstance().getListRuta(context);
		}
		return listFilteredRoutes;
	}
	public List<Ruta> getListadoRutas(Context context) {
		return GestorBaseDeDatos.getInstance().getListRuta(context);
	}
	
	 /**
	  * It filters the routes by the locations and it stores the filtered list in memory. 
	  * It is recommended to use this method on a separated thread
	  * @param context
	  * @param handler 
	  * @return
	  */
	public void getFilteredRoutesByLocation(Context context,FilteringHandler handler){
		listFilteredRoutes = new ArrayList<Ruta>();
		//SQL do not support acos or other functions 
		//to improve the performance using direct query (e.g. join).
		if(locationOrigin != null && locationDestination != null){
			float[] resultsOrigin = new float[1];
			float[] resultsDestination = new float[1];
			float calculatedRadiusFromOrigin;
			float calculatedRadiusFromDestination;
			boolean isPassingByOrigin;
			boolean isPassingByDestination;
			List<Empresa> listadoEmpresas = GestorBaseDeDatos.getInstance().getListEmpresa(context);
			for (int i = 0; i < listadoEmpresas.size(); i++) {
				List<Ruta> listRoutes = listadoEmpresas.get(i).getRutaList();
				for (int j = 0; j < listRoutes.size(); j++) {
					List<Punto> listPoints = listRoutes.get(j).getPuntoList();
					isPassingByOrigin = false;
					isPassingByDestination = false;
					for (int k = 0; k < listPoints.size(); k++) {
						Punto point = listPoints.get(k);
						Location.distanceBetween( locationOrigin.latitude, 
								locationOrigin.longitude, 
								point.getLatitud(), point.getLongitud(), resultsOrigin);
						calculatedRadiusFromOrigin = resultsOrigin[0];
						Location.distanceBetween( locationDestination.latitude, 
								locationDestination.longitude, 
								point.getLatitud(), point.getLongitud(), resultsDestination);
						calculatedRadiusFromDestination = resultsDestination[0];
						if(calculatedRadiusFromOrigin < visibleRoutesZoneRadiusFromOrigin){
							isPassingByOrigin = true;
							if(isPassingByDestination){
								break;
							}
						}
						if(calculatedRadiusFromDestination 
								< visibleRoutesZoneRadiusFromDestination){
							isPassingByDestination = true;
							if(isPassingByOrigin){
								break;
							}
						}
					}
					if(isPassingByOrigin && isPassingByDestination){
						//route is near origin and destination
						listFilteredRoutes.add(listRoutes.get(j));
						break;
					}
				}
			}
		}
		handler.onFinishFiltering();
	}
	public interface FilteringHandler{
		public void onFinishFiltering();
	}
	public List<Long> getSelectedEmpresaIds() {
		return selectedEmpresaIds;
	}
	public void setSelectedEmpresas(List<Long> selectedEmpresaIds) {
		this.selectedEmpresaIds = selectedEmpresaIds;
	}
	public void setSelectedRutas(List<Ruta> selectedRutas) {
		this.selectedRutas = selectedRutas;
	}
	public List<Ruta> getSelectedRutas() {
		return selectedRutas;
	}
	public LatLng getMyLocation() {
		return myLocation;
	}
	public void setMyLocation(LatLng myLocation) {
		this.myLocation = myLocation;
	}
	
	Random randomGenerator = new Random();
	//avoid problematic colors for maps
	//http://en.wikipedia.org/wiki/HSL_and_HSV#HSV
	final int HUE_MIN_VAL = 0;
	final int HUE_TOP_VAL = 360;
	final int HUE_MAX_VAL = 360;
	final int 	SATURATION_MIN_VAL = 75;
	final int 	SATURATION_TOP_VAL = 100;
	final float SATURATION_MAX_VAL = 100f;
	final int 	VALUE_MIN_VAL = 25;
	final int 	VALUE_TOP_VAL = 87;
	final float VALUE_MAX_VAL = 100f;
	final int HUE_NEW_CALC_TOP_VAL = HUE_TOP_VAL - HUE_MIN_VAL;
	final int SATURATION_NEW_CALC_TOP_VAL = (int)(SATURATION_TOP_VAL - SATURATION_MIN_VAL);
	final int VALUE_NEW_CALC_TOP_VAL = (int)(VALUE_TOP_VAL - VALUE_MIN_VAL);
	final int[] HUE_PROBLEMATIC_VAL = {45, 65, 180, 300};
	final int HUE_PROBLEMATIC_RANGE_VAL = 10;
	public int getRandomColor() {
		//Hue [0 .. 360) Saturation [0...1] is Value [0...1]
		float hue = randomGenerator.nextInt(HUE_NEW_CALC_TOP_VAL) + HUE_MIN_VAL;
		float saturation = 
				(randomGenerator.nextInt(SATURATION_NEW_CALC_TOP_VAL) + SATURATION_MIN_VAL) 
				/ SATURATION_MAX_VAL;
		float value = 
				(randomGenerator.nextInt(VALUE_NEW_CALC_TOP_VAL) + VALUE_MIN_VAL) 
				/ VALUE_MAX_VAL;
		
		//avoid problematic colors for maps
		//http://en.wikipedia.org/wiki/HSL_and_HSV#HSV
		//http://www.yafla.com/yaflacolor/ColorRGBHSL.html
		for(int i = 0; i < HUE_PROBLEMATIC_VAL.length; i++){
			while(hue > (HUE_PROBLEMATIC_VAL[i] - HUE_PROBLEMATIC_RANGE_VAL) 
					&& (hue < HUE_PROBLEMATIC_VAL[i] + HUE_PROBLEMATIC_RANGE_VAL)){
				hue = randomGenerator.nextInt(HUE_NEW_CALC_TOP_VAL) + HUE_MIN_VAL;
			}
		}
		
		float[] hsv = { hue, saturation, value };
		int alpha = 0xA9;//00 transparent FF fill
		return Color.HSVToColor(alpha, hsv);
	}
	public int getVisibleRoutesZoneRadiusFromOrigin() {
		return visibleRoutesZoneRadiusFromOrigin;
	}
	public void setVisibleRoutesZoneRadiusFromOrigin(
			int visibleRoutesZoneRadiusFromOrigin) {
		this.visibleRoutesZoneRadiusFromOrigin = visibleRoutesZoneRadiusFromOrigin;
	}
	public int getVisibleRoutesZoneRadiusFromDestination() {
		return visibleRoutesZoneRadiusFromDestination;
	}
	public void setVisibleRoutesZoneRadiusFromDestination(
			int visibleRoutesZoneRadiusFromDestination) {
		this.visibleRoutesZoneRadiusFromDestination = visibleRoutesZoneRadiusFromDestination;
	}
	public LatLng getLocationOrigin() {
		return locationOrigin;
	}
	public void setLocationOrigin(LatLng origin) {
		this.locationOrigin = origin;
	}
	public LatLng getLocationDestination() {
		return locationDestination;
	}
	public void setLocationDestination(LatLng destination) {
		this.locationDestination = destination;
	}
}
