package com.devos.rutasmapas.model.bean;

import java.util.List;
import com.devos.rutasmapas.model.bean.DaoSession;
import de.greenrobot.dao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
import java.util.ArrayList;
// KEEP INCLUDES END
/**
 * Entity mapped to table EMPRESA.
 */
public class Empresa {

    private Long id;
    private Integer agenciaId;
    private String agenciaNombre;
    private String agenciaNombreReal;
    private String agenciaUrl;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient EmpresaDao myDao;

    private List<Ruta> rutaList;

    // KEEP FIELDS - put your custom fields here
    private boolean selected;
    // KEEP FIELDS END

    public Empresa() {
    }

    public Empresa(Long id) {
        this.id = id;
    }

    public Empresa(Long id, Integer agenciaId, String agenciaNombre, String agenciaNombreReal, String agenciaUrl) {
        this.id = id;
        this.agenciaId = agenciaId;
        this.agenciaNombre = agenciaNombre;
        this.agenciaNombreReal = agenciaNombreReal;
        this.agenciaUrl = agenciaUrl;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getEmpresaDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAgenciaId() {
        return agenciaId;
    }

    public void setAgenciaId(Integer agenciaId) {
        this.agenciaId = agenciaId;
    }

    public String getAgenciaNombre() {
        return agenciaNombre;
    }

    public void setAgenciaNombre(String agenciaNombre) {
        this.agenciaNombre = agenciaNombre;
    }

    public String getAgenciaNombreReal() {
        return agenciaNombreReal;
    }

    public void setAgenciaNombreReal(String agenciaNombreReal) {
        this.agenciaNombreReal = agenciaNombreReal;
    }

    public String getAgenciaUrl() {
        return agenciaUrl;
    }

    public void setAgenciaUrl(String agenciaUrl) {
        this.agenciaUrl = agenciaUrl;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<Ruta> getRutaList() {
        if (rutaList == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            RutaDao targetDao = daoSession.getRutaDao();
            List<Ruta> rutaListNew = targetDao._queryEmpresa_RutaList(id);
            synchronized (this) {
                if(rutaList == null) {
                    rutaList = rutaListNew;
                }
            }
        }
        return rutaList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetRutaList() {
        rutaList = null;
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

    // KEEP METHODS - put your custom methods here
    public List<Ruta> getRutas(){
    	if(rutaList != null){
    		rutaList = new ArrayList<Ruta>();
    	}
    	return rutaList;
    }
    public void setRutas(List<Ruta> rutaList){
    	this.rutaList = rutaList;
    }
    
    public boolean isSelected() {
    	return selected;
    }
    
    public void setSelected(boolean selected) {
    	this.selected = selected;
    }
    // KEEP METHODS END

}
