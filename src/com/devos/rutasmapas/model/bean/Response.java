package com.devos.rutasmapas.model.bean;

public class Response {
	private MessageDetail message;
    private boolean success;
    
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public MessageDetail getMessage() {
		return message;
	}
	public void setMessage(MessageDetail message) {
		this.message = message;
	}
	
	public static class MessageDetail {
		private String message;
		private String id;
		
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
	}
}

