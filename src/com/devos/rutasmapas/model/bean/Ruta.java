package com.devos.rutasmapas.model.bean;

import java.util.List;
import com.devos.rutasmapas.model.bean.DaoSession;
import de.greenrobot.dao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
import java.util.ArrayList;
// KEEP INCLUDES END
/**
 * Entity mapped to table RUTA.
 */
public class Ruta {

    private Long id;
    private Integer rutaId;
    private String rutaNombre;
    private String rutaTrazada;
    private Integer color;
    private long empresaId;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient RutaDao myDao;

    private Empresa empresa;
    private Long empresa__resolvedKey;

    private List<Punto> puntoList;

    // KEEP FIELDS - put your custom fields here
    private ArrayList<Double> puntos;
    private boolean selected;
    // KEEP FIELDS END

    public Ruta() {
    }

    public Ruta(Long id) {
        this.id = id;
    }

    public Ruta(Long id, Integer rutaId, String rutaNombre, String rutaTrazada, Integer color, long empresaId) {
        this.id = id;
        this.rutaId = rutaId;
        this.rutaNombre = rutaNombre;
        this.rutaTrazada = rutaTrazada;
        this.color = color;
        this.empresaId = empresaId;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getRutaDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRutaId() {
        return rutaId;
    }

    public void setRutaId(Integer rutaId) {
        this.rutaId = rutaId;
    }

    public String getRutaNombre() {
        return rutaNombre;
    }

    public void setRutaNombre(String rutaNombre) {
        this.rutaNombre = rutaNombre;
    }

    public String getRutaTrazada() {
        return rutaTrazada;
    }

    public void setRutaTrazada(String rutaTrazada) {
        this.rutaTrazada = rutaTrazada;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public long getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(long empresaId) {
        this.empresaId = empresaId;
    }

    /** To-one relationship, resolved on first access. */
    public Empresa getEmpresa() {
        long __key = this.empresaId;
        if (empresa__resolvedKey == null || !empresa__resolvedKey.equals(__key)) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            EmpresaDao targetDao = daoSession.getEmpresaDao();
            Empresa empresaNew = targetDao.load(__key);
            synchronized (this) {
                empresa = empresaNew;
            	empresa__resolvedKey = __key;
            }
        }
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        if (empresa == null) {
            throw new DaoException("To-one property 'empresaId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.empresa = empresa;
            empresaId = empresa.getId();
            empresa__resolvedKey = empresaId;
        }
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<Punto> getPuntoList() {
        if (puntoList == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            PuntoDao targetDao = daoSession.getPuntoDao();
            List<Punto> puntoListNew = targetDao._queryRuta_PuntoList(id);
            synchronized (this) {
                if(puntoList == null) {
                    puntoList = puntoListNew;
                }
            }
        }
        return puntoList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetPuntoList() {
        puntoList = null;
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

    // KEEP METHODS - put your custom methods here
	public ArrayList<Double> getPuntos() {
		return puntos;
	}

	public void setPuntos(ArrayList<Double> puntos) {
		this.puntos = puntos;
	}
	
	public boolean isSelected() {
    	return selected;
    }
    
    public void setSelected(boolean selected) {
    	this.selected = selected;
    }
    // KEEP METHODS END

}
