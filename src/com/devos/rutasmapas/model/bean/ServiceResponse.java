package com.devos.rutasmapas.model.bean;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceResponse {
	Response response = new Response();

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response mensaje) {
		this.response = mensaje;
	}
	
	public static class EmpresaResponse extends ServiceResponse {
		private List<Empresa> data = new ArrayList<Empresa>();

		public List<Empresa> getData() {
			return data;
		}

		void setData(List<Empresa> data) {
			this.data = data;
		}
	}
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static interface EmpresaFilter {
		//agencia_id es el identificador que se usara para actualizar los demas campos.
		@JsonProperty("agencia_id") abstract Long getId(); // rename property
		@JsonProperty("agencia_nombre") abstract int getAgenciaNombre(); 
		@JsonProperty("agencia_nombre_real") abstract int getAgenciaNombreReal(); 
		@JsonProperty("agencia_url") abstract int getAgenciaUrl(); 
	}
	public static interface RutaFilter {
		@JsonProperty("ruta_id") abstract int getRutaId();
		@JsonProperty("ruta_nombre") abstract int getRutaNombre();
		@JsonProperty("ruta_trazada") abstract int getRutaTrazada();
	}
}
