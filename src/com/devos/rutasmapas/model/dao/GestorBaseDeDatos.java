package com.devos.rutasmapas.model.dao;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.devos.rutasmapas.model.GestorPrincipal;
import com.devos.rutasmapas.model.bean.Configuracion;
import com.devos.rutasmapas.model.bean.ConfiguracionDao;
import com.devos.rutasmapas.model.bean.DaoMaster;
import com.devos.rutasmapas.model.bean.DaoMaster.DevOpenHelper;
import com.devos.rutasmapas.model.bean.DaoSession;
import com.devos.rutasmapas.model.bean.Empresa;
import com.devos.rutasmapas.model.bean.EmpresaDao;
import com.devos.rutasmapas.model.bean.Punto;
import com.devos.rutasmapas.model.bean.PuntoDao;
import com.devos.rutasmapas.model.bean.Ruta;
import com.devos.rutasmapas.model.bean.RutaDao;
import com.devos.rutasmapas.model.bean.RutaDao.Properties;


public class GestorBaseDeDatos {
	private static GestorBaseDeDatos gestor = new GestorBaseDeDatos();

	public Context context;

	private SQLiteDatabase db;
	private DaoMaster daoMaster;
	private DaoSession daoSession;
	
	public static GestorBaseDeDatos getInstance() {
		return gestor;
	}
	//TODO asegurar que nunca se pierda el contexto a traves de diferentes activities.
	public void inicializarDB(Context context) {
		this.context = context;
		DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, GestorPrincipal.DB_NAME, null);
		db = helper.getWritableDatabase(); 
	}

	private void initSession(Context context){
		if(db == null){
			inicializarDB(context);
		}
		daoMaster = new DaoMaster(db);
    	daoSession = daoMaster.newSession();
	}
	/**
	 * Para el caso de trabajar con base de datos inicial prellenada. 
	 * @throws IOException
	 */
	public void copyDatabase() throws IOException {
		InputStream myInput = context.getAssets().open(GestorPrincipal.DB_NAME_FROM_ASSETS);
		String outFileName = GestorPrincipal.DB_PATH + GestorPrincipal.DB_NAME;
		OutputStream myOutput = new FileOutputStream(outFileName);
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}
		myOutput.flush();
		myOutput.close();
		myInput.close();

	}
	//TODO (opcional) se puede extraer el manejo de mSettings
	/**
	 * Recupera los valores de configuracion de la aplicacion. 
	 * Caso contrario, estable valores por defecto.
	 */
	public void setupConfiguracionInicial(Context context) {
		Configuracion configuracion = new Configuracion();
    	initSession(context);
    	ConfiguracionDao configuracioDao = daoSession.getConfiguracionDao();
    	
    	List<Configuracion> listConfiguracion = configuracioDao.queryBuilder().list();
    	if(listConfiguracion.size() == 1){
    		configuracion = listConfiguracion.get(0);
    	}else{
    		configuracion.setHost(GestorPrincipal.CONFIG_DEFAULT_HOST);
	    	configuracion.setPort(GestorPrincipal.CONFIG_DEFAULT_PORT);
	    	configuracion.setFechaActualizacion(GestorPrincipal.CONFIG_DEFAULT_FECHA);
    	}
	    GestorPrincipal.getInstance().setConfiguracion(configuracion);
	}
	public void saveConfiguracion(Configuracion configuracion, Context context){
		initSession(context);
		ConfiguracionDao conf = daoSession.getConfiguracionDao();
		conf.deleteAll();
		conf.insert(configuracion);	
	}
	
	public Configuracion getConfiguracion(Context context){
		initSession(context);
		Configuracion configuracion = null;	
    	ConfiguracionDao configuracioDao = daoSession.getConfiguracionDao();
    	List<Configuracion> listConfiguracion = configuracioDao.queryBuilder().list();
    	if(listConfiguracion.size() == 1){
    		configuracion = listConfiguracion.get(0);
    	}
		return configuracion;
	}
	
	public void saveListadoEmpresa(final List<Empresa> listadoEmpresa){
		//Al momento de guardar lista anidadas,
		//primero se tiene que asignar el id de la entidad maetro a las listas detalle.
		initSession(context);
		List<Ruta> listadoRuta = new ArrayList<Ruta>();
		long idEmpresa = 0;
		long idRuta = 0;
		
		EmpresaDao empresaDao = daoSession.getEmpresaDao();
		RutaDao rutaDao = daoSession.getRutaDao();
		PuntoDao puntoDao = daoSession.getPuntoDao();
		empresaDao.insertOrReplaceInTx(listadoEmpresa);
		rutaDao.deleteAll();
		puntoDao.deleteAll();
		
		for(int i = 0 ;i < listadoEmpresa.size() ; i++){
			idEmpresa = listadoEmpresa.get(i).getId();
			listadoRuta = listadoEmpresa.get(i).getRutaList();
			int size = listadoRuta.size();
			for(int j = 0 ;j < size ; j++){
				Ruta ruta = listadoRuta.get(j);
				ruta.setEmpresaId(idEmpresa);
				rutaDao.insertOrReplaceInTx(ruta);
				
				idRuta = ruta.getId();
				int sizek = ruta.getPuntos().size();
				double lat = 0;
				double lng;
				List<Punto> listadoPunto = new ArrayList<Punto>();
				for (int k = 0; k < sizek; k++) {
					if( ( k + 1 ) % 2 == 0){
						lng = ruta.getPuntos().get(k);
						Punto punto = new Punto(lat, lng);
						punto.setRutaId(idRuta);
						listadoPunto.add(punto);
					}else{
						lat = ruta.getPuntos().get(k);
					}
				}
				puntoDao.insertOrReplaceInTx(listadoPunto);
			}
		}
	}
	public void saveListRuta(Context context,List<Ruta> list){
		initSession(context);
		RutaDao rutaDao = daoSession.getRutaDao();
		rutaDao.insertOrReplaceInTx(list);
	}
	public List<Empresa> getListEmpresa(Context context){
		initSession(context);
		List<Empresa> listadoEmpresas= new ArrayList<Empresa>();
		
    	EmpresaDao empresaDao = daoSession.getEmpresaDao();
    	listadoEmpresas = empresaDao.queryBuilder().list();
		return listadoEmpresas;
	}
	public List<Ruta> getListRuta(Context context,List<Long> empresasId){
		initSession(context);
		List<Ruta> listadoRuta= new ArrayList<Ruta>();
		
		RutaDao rutaDao = daoSession.getRutaDao();
		listadoRuta = rutaDao.queryBuilder()
				.where(Properties.EmpresaId.in(empresasId))
				.list();
		return listadoRuta;
	}
	public List<Ruta> getListRuta(Context context){
		String RUTA_TRAZADA_TRUE = "1";
		initSession(context);
		List<Ruta> listadoRuta= new ArrayList<Ruta>();
		
		RutaDao rutaDao = daoSession.getRutaDao();
		listadoRuta = rutaDao.queryBuilder()
				.where(Properties.RutaTrazada.like(RUTA_TRAZADA_TRUE))
				.list();
		return listadoRuta;
	}
}
