package com.devos.rutasmapas.model.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;

import android.content.Context;
import android.net.Uri;

import com.devos.rutasmapas.model.GestorPrincipal;
import com.devos.rutasmapas.model.bean.Empresa;
import com.devos.rutasmapas.model.bean.Ruta;
import com.devos.rutasmapas.model.bean.ServiceResponse;
import com.devos.rutasmapas.model.bean.ServiceResponse.EmpresaResponse;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class GestorSincronizacion {
	private static final GestorSincronizacion gestor = new GestorSincronizacion();
	
	public static enum EstadoSincronizacion {
		CONECTIVIDAD_SIN_CONEXION, 
		CONECTIVIDAD_SIN_PAQUETE_DE_DATOS, 
		CONECTIVIDAD_OK, 
		CONECTIVIDAD_ERROR_HOST_NULL, 
		SINCRONIZACION_EN_PROGRESO, 
		SINCRONIZACION_COMPLETADA
	}
	public static final String HOST_NULL = "null";
	public static final String HOST_NOT_CATCHED = "NOT CATCHED";
	public static final String PORT_NULL = "null";
	public static final String PORT_EMPTY = "";
	public static final String SEPARATOR_HOST_PORT = ":";
	public static final String HTTP = "http://";
	public static final String HTTPS = "https://";
	
	private ConectividadListener conectividadListener;
	private RespuestaSincronizacionHandler respuestaSincronizacionHandler;
	private EstadoSincronizacion estadoSincronizacion;
	private int elementosSincronizados;
	private int elementosSincronizadosCorrectamente;
	private int totalElementosASincronizar = GestorPrincipal.SERVICE_REQUESTS;
	private static AsyncHttpClient client = new AsyncHttpClient();

	private String basePath;

	public GestorSincronizacion() {
	}

	public static GestorSincronizacion getInstance() {
		return gestor;
	}

	public boolean isConectado() {
		return conectividadListener.isConectado();
	}

	public int getElementosSincronizados() {
		return elementosSincronizados;
	}

	public int getElementosSincronizadosCorrectamente() {
		return elementosSincronizadosCorrectamente;
	}

	public int getTotalElementosASincronizar() {
		return totalElementosASincronizar;
	}

	public void inicializarSincronizacion(
			ConectividadListener conectividadListener,
			RespuestaSincronizacionHandler respuestaSincronizacionHandler) {
		this.conectividadListener = conectividadListener;
		this.respuestaSincronizacionHandler = respuestaSincronizacionHandler;
	}

	public void iniciarSincronizacionInterna(final Context context){
		    new Thread(new Runnable() {
		        public void run() {
		        	try {
		        		BufferedReader reader = new BufferedReader(
		    		        new InputStreamReader(context.getAssets()
		    		        		.open(GestorPrincipal.DATA_FILENAME)));
		    		    // do reading, usually loop until end of file reading  
		    		    String mLine = reader.readLine();
		    		    StringBuilder content = new StringBuilder();
		    		    while (mLine != null) {
		    			   content.append(mLine);
		    		       mLine = reader.readLine(); 
		    		    }
		    		    content.append(mLine);
		    		    reader.close();
		    		    procesarRespuesta(ServiceResponse.EmpresaResponse.class, content.toString());
		        	} catch (IOException e) {
		        		e.printStackTrace();
		        	}
		        	estadoSincronizacion = EstadoSincronizacion.SINCRONIZACION_COMPLETADA;
					respuestaSincronizacionHandler.finalizarSincronizacion();
		        }
		    }).start();
		
	}
	
	public void iniciarSincronizacion(Context context) {
		String port = GestorPrincipal.getInstance().getConfiguracion().getPort();
		String host = GestorPrincipal.getInstance().getConfiguracion().getHost();
		basePath = getURL(port, host);

		if (!(context instanceof ConectividadListener)) {
			throw new ClassCastException(context.toString()
					+ " must implement ConectividadListener");
		}
		if (!(context instanceof RespuestaSincronizacionHandler)) {
			throw new ClassCastException(context.toString()
					+ " must implement AsyncHttpResponseHandler");
		}
		if (conectividadListener == null) {
			throw new NullPointerException(context.toString()
					+ " must instantiate ConectividadListener");
		}
		if (respuestaSincronizacionHandler == null) {
			throw new NullPointerException(context.toString()
					+ " must instantiate AsyncHttpResponseHandler");
		}
		// TODO incluir como parte de validacion de la libreria (opcional)
		Uri uri = Uri.parse(basePath);
		if (uri.getHost().equalsIgnoreCase(HOST_NULL)
				|| uri.getHost().equalsIgnoreCase(HOST_NOT_CATCHED)) {
			estadoSincronizacion = EstadoSincronizacion.CONECTIVIDAD_ERROR_HOST_NULL;
			respuestaSincronizacionHandler.finalizarSincronizacion();
			return;
		}
		if (conectividadListener.isConectado()) {
			if (!conectividadListener.isConectadoSinPaqueteDeDatos()) {
				sincronizar(context);
			} else {
				estadoSincronizacion = EstadoSincronizacion.CONECTIVIDAD_SIN_PAQUETE_DE_DATOS;
				respuestaSincronizacionHandler.finalizarSincronizacion();
			}
		} else {
			estadoSincronizacion = EstadoSincronizacion.CONECTIVIDAD_SIN_CONEXION;
			respuestaSincronizacionHandler.finalizarSincronizacion();
		}
	}

	// TODO desacoplar sincronizacion de consultas hacia cloud
	// TODO evaluar multi threading requests.
	private void sincronizar(Context context) {
		elementosSincronizados = 0;
		Header headers[] = null;
		String requestJSON = String.format(GestorPrincipal.SERVICE_REQUEST_BODY,
				GestorPrincipal.SERVICE_API_KEY, GestorPrincipal.getInstance()
				.getConfiguracion().getFechaActualizacion());
		StringEntity stringEntity;
		try {
			stringEntity = new StringEntity(requestJSON, GestorPrincipal.SERVICE_CLIENT_CHARSET);
			// TODO modificar la libreria para que soporte multithreading
			client.setTimeout(GestorPrincipal.SERVICE_CLIENT_TIMEOUT);
			client.post(context,
					getAbsoluteUrl(GestorPrincipal.SERVICE_REQUEST_URL_EMPRESAS),
					headers, stringEntity,
					GestorPrincipal.SERVICE_CLIENT_CONTENT_TYPE,
					new SincronizacionAsyncHttpResponseHandler() {
						@Override
						public void onSuccess(String response) {
							onSuccess(ServiceResponse.EmpresaResponse.class,
									response);
						}
					});
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	public <claseResponse> void procesarRespuesta(Class<claseResponse> claseResponse, String respuesta){
		ObjectMapper mapper = new ObjectMapper();
		mapper.addMixInAnnotations(Empresa.class, ServiceResponse.EmpresaFilter.class);
		mapper.addMixInAnnotations(Ruta.class, ServiceResponse.RutaFilter.class);
		try { 
			claseResponse response = mapper.readValue(respuesta,claseResponse);
			if(response instanceof ServiceResponse){
				if(((ServiceResponse)response).getResponse().isSuccess()){
					if(response instanceof EmpresaResponse){
						List<Empresa> data = ((EmpresaResponse)response).getData();				
						if(data != null && data.size() > 0){
							GestorBaseDeDatos.getInstance().saveListadoEmpresa(data);
						}
					}
					elementosSincronizadosCorrectamente++;
				}
			}
		}catch(NullPointerException e){
			//when the response is null
			e.printStackTrace();
		}catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public EstadoSincronizacion getEstadoSincronizacion() {
		return estadoSincronizacion;
	}

	public void setEstadoSincronizacion(
			EstadoSincronizacion estadoSincronizacion) {
		this.estadoSincronizacion = estadoSincronizacion;
	}

	public interface ConectividadListener {
		public boolean isConectado();

		public boolean isConectadoSinPaqueteDeDatos();
	}

	public interface RespuestaSincronizacionHandler {
		public void finalizarSincronizacion();
	}

	public class SincronizacionAsyncHttpResponseHandler extends
			AsyncHttpResponseHandler {
		boolean success;
		public <T> void onSuccess(final Class<T> clase, final String response) {
			success = true;
			new Thread(new Runnable() {
		        public void run() {
		        	procesarRespuesta(clase, response);
		        	onFinalFinish();
		        }
		    }).start();
		}
		@Override
		public void onFinish() {
			elementosSincronizados++;
			if(!success){
				onFinalFinish();
			}
		}
		public void onFinalFinish() {
			if (elementosSincronizados == totalElementosASincronizar) {
				estadoSincronizacion = EstadoSincronizacion.SINCRONIZACION_COMPLETADA;
				respuestaSincronizacionHandler.finalizarSincronizacion();
			}
		}
	}
	private String getAbsoluteUrl(String relativeUrl) {
		return basePath + GestorPrincipal.SERVICE_APP + relativeUrl;
	}
	public static String getURL(String port, String host){
		//TODO mejorar el manejo de este metodo y sus constantes.
		if(port == null || port.equalsIgnoreCase(PORT_NULL) 
				|| port.equalsIgnoreCase(PORT_EMPTY)){
			return HTTP + host;
		}
		return HTTP + host + SEPARATOR_HOST_PORT + port;
	}
}
