package com.devos.tools;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class ProjectGreenDaoGenerator {
	public final static String SEPARATOR_FOLDER = ".";
	public final static String PROJECT_PACKAGE_NAME = "com.devos.rutasmapas";
	public final static String FOLDER_GENERATED_BEAN = "model.bean";
	public final static String SCHEMA_JAVA_PACKAGE = PROJECT_PACKAGE_NAME + SEPARATOR_FOLDER 
			+ FOLDER_GENERATED_BEAN;
	public final static int SCHEMA_VERSION = 5;
	public final static String OUTPUT_DIRECTORY = "../RutasMapas/src";
	/**
	 * documentation http://greendao-orm.com/javadoc/greendao-generator/
	 * Agregar en Java Build Path / Projects, DaoGenerator.
	 * Para su ejecucion recordar configurar en eclipse "Run Configurations...",  
	 * quitar "Android x.x" de Classpath/"Bootstrap Entries"
	 * en "User Entries" comprobar que esten RutasMapas, DaoGenerator y demas librerias necesarias. 
	 * Se puede usar el boton Restore Default pero considerando los puntos anteriores. 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		Schema schema = new Schema(SCHEMA_VERSION, SCHEMA_JAVA_PACKAGE);
		schema.enableKeepSectionsByDefault();
		addEmpresa(schema);
		addConfiguracion(schema);
		
		new DaoGenerator().generateAll(schema, OUTPUT_DIRECTORY);
	}

	private static void addConfiguracion(Schema schema) {
		Entity configuracion = schema.addEntity("Configuracion");
		configuracion.addIdProperty();
		configuracion.addStringProperty("host");
		configuracion.addStringProperty("port");
		configuracion.addStringProperty("fechaActualizacion");
	}
	
	private static void addEmpresa(Schema schema) {
		Entity empresa = schema.addEntity("Empresa");
		empresa.addIdProperty();
		empresa.addIntProperty("agenciaId");
		empresa.addStringProperty("agenciaNombre");
		empresa.addStringProperty("agenciaNombreReal");
		empresa.addStringProperty("agenciaUrl");

		Entity ruta = schema.addEntity("Ruta");
		ruta.addIdProperty();
		ruta.addIntProperty("rutaId");
		ruta.addStringProperty("rutaNombre");
		ruta.addStringProperty("rutaTrazada");
		ruta.addIntProperty("color");
		
		Property idEmpresa = ruta.addLongProperty("empresaId").notNull().getProperty();
		ruta.addToOne(empresa, idEmpresa);
		empresa.addToMany(ruta,idEmpresa);
		
		Entity punto = schema.addEntity("Punto");
		punto.addIdProperty();
		punto.addDoubleProperty("latitud");
		punto.addDoubleProperty("longitud");
		
		Property idRuta = punto.addLongProperty("rutaId").notNull().getProperty();
		punto.addToOne(ruta, idRuta);
		ruta.addToMany(punto,idRuta);
	}
}